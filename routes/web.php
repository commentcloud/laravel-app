<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CommentController@index');
//Route::get('/aj_get', 'CommentController@aj_get');
Route::post('/aj_get', 'CommentController@aj_get');
Route::post('/aj_add', 'CommentController@aj_add');
Route::post('/aj_update_last_used', 'CommentController@aj_update_last_used');
Route::post('/aj_delete', 'CommentController@aj_delete');
Route::post('/aj_edit', 'CommentController@aj_edit');

Route::get('/comments/{comment}', 'CommentController@show');



Auth::routes();

Route::get('/home', 'HomeController@index');
