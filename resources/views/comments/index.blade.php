@extends ('layouts.page')
@section ('head')
<script src="/js/comment.js"></script>
<script src="/js/magnific.js"></script>
<link href="/css/magnific.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/js/jquery.hotkeys.js"></script>
<script type="text/javascript" src="/js/jquery.ns-autogrow.min.js"></script>
@endsection
@section ('content')
<div id="app">
    <div class="row">
        <div class="col-6">

            <?php
            echo Form::open();
            ?>
            <input type="text" name="input" autocomplete="off" class="main-input" placeholder="Search for a previous comment or start typing a new one">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <?php
            $array = array(
                'class' => 'btn btn-secondary submit',
                'data-toggle' => "tooltip",
                'data-placement' => "bottom",
                'title' => "Add to your Cloud"
                );
            echo Form::button('<i class="fa fa-plus" aria-hidden="true"></i>',$array);
            echo Form::close();
            ?>
            <section id="last-used">
            </section>
            <img src="/img/ring.gif" class="loader" style="width:45px;height:45px;margin:0 auto;display:none"/>
        </div>
        <div class="col-6">
            <section id="builder">
                <a href="#pasta-popup" class="pastalink open-popup-link btn btn-primary" style="display:none">Debug</a>
                <a href="#" class="copylink btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Copy to Clipboard"><i class="fa fa-files-o" aria-hidden="true"></i></a>
                <a href="#" class="reset btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Reset"><i class="fa fa-refresh" aria-hidden="true"></i></a>
                <a href="#" class="undo btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Undo" style="display:none"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></a>
                <div class="feedback">
                    <aside id="positive">
                        <h4 style="display:none">You have shown that you can:</h4>
                    </aside>
                    <aside id="constructive">
                        <h4 style="display:none">To improve, you should:</h4>
                    </aside>
                </div>
            </section>
        </div>
    </div>
</div>
<div id="pasta-popup" class="white-popup mfp-hide">
    <textarea id="pasta">
        test
    </textarea>
</div>
@endsection
