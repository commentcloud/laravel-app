var selectedIndex = -1;
var archive = '';
$(document).ready(function () {
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
    retrieveComments(undefined);
    selectIndex(0);
    var timeout;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).keydown(function (e) {
        clearTooltips();
        if (e.ctrlKey) {
            switch (e.which) {
                case 67:
                    //ctrl+c
                    $('.copylink').click();
                    break;
                case 82:
                    //ctrl+r
                    $('.reset').click();
                    break;
                case 90:
                    //ctrl+z
                    if($('.undo').is(":visible")){
                        $('.undo').click();
                    }
                    break;

                default:
                    return; // exit this handler for other keys
            }
        } else {
            var inputbox = $("input[name=input]");
            switch (e.which) {
                case 37:
                    //left
                    if (inputbox.is(":focus")) {
                        //allow normal cursor movement 
                        return;
                    } else {
                        if (selectedIndex > 0) {
                            selectType('positive');
                        }
                    }
                    break;

                case 38: // up
                    selectIndex(selectedIndex - 1);
                    break;

                case 39: // right
                    if (inputbox.is(":focus")) {
                        var currentpos = inputbox.getCursorPosition();
                        var inputlength = inputbox.val().length;
                        if (currentpos === inputlength) {
                            $(".btn.submit").focus();
                        } else {
                            //allow normal cursor movement if we're not at the end.
                            return;
                        }
                    } else {
                        if (selectedIndex > 0) {
                            selectType('constructive');
                        } else {
                            inputbox.focus();
                        }
                    }
                    break;

                case 40: // down
                    selectIndex(selectedIndex + 1);
                    break;

                case 13: //enter
                    var builderFocus = submitFocusedBuilder();

                    if (!builderFocus) {
                        if (selectedIndex > 0) {
                            var highlighted = $("#last-used .comment.highlighted");
                            if (highlighted.hasClass('being-edited')) {
                                saveChanges(highlighted.data('id'));
                            } else {
                                addHighlighted();
                            }
                        } else {
                            if ($('.btn.submit').is(":focus")) {
                                $('.btn.submit').click();
                            }
                        }
                    }
                    break;

                default:
                    return; // exit this handler for other keys
            }
        }
        e.preventDefault(); // prevent the default action (scroll / move caret)
    });
    $(document).bind('keydown', 'alt+c', function () {
        //copyToClipboard();
    });
    $(document).bind('keydown', 'ctrl+r', function () {
        $(".reset").click();
    });

    $('.copylink').on('click', function () {
        $('#pasta').val(generatePasta());
        copyToClipboard();
    });

    /*redundant?
     $("#last-used .comment"). click(function(){
     if(!$(this).hasClass('being-edited')){
     var id = $(this).data('id');
     selectIndexById(id);
     }
     });
     */
    $("input.main-input").click(function () {
        selectIndex(0);
    });

    $('.btn.submit').click(function (event) {
        event.preventDefault();
        var comment = $('input[name="input"]').val();
        //var type = $(this).val();
        var formdata = {input: comment};
        $(".loader").show();
        $.post("/aj_add", formdata)
                .done(function (data) {
                    var tooltip = '<strong>Your Comment has now been added to your Cloud.</strong> \n\
                        <br>You can now add it to your feedback builder using the icons to the left and right<br>\n\
                        (or the <strong>left</strong> and <strong>right</strong> buttons on your keyboard)';
                    retrieveComments(comment, true, tooltip);
                });
    });
    $('input[name="input"]').keyup(function (e) {
        if (e.which !== 37 && e.which !== 38 && e.which !== 39 && e.which !== 40) {
            $(".loader").show();
            clearTimeout(timeout);

            var search = $(this).val().toUpperCase();
            var ids = addedIds();
            $('#last-used .comment').each(function () {
                var comment = $(this).children('textarea').html().toUpperCase();

                if (comment.includes(search) && ids.indexOf($(this).data('id')) == -1) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });

            timeout = setTimeout(function () {
                retrieveComments(search);
            }, 300);
        }
    });


    $(document).on("click", "#last-used .comment:not(.being-edited) textarea", function (event) {
        var id = $(this).parent().data('id');
        var i = 1;
        $("#last-used .comment:visible").each(function () {
            if ($(this).data('id') === id) {
                selectIndex(i);
            } else {
                i = i + 1;
            }
        });
    });
    $(document).on("click", '#last-used .comment .add-to-builder', function (event) {
        clearTooltips();
        addToBuilder($(this).parent().data('id'), $(this).parent().children('textarea').html(), $(this).data('type'));
        removeFromSearch();
        emptyInput();
        updateLastUsed($(this).parent().data('id'));
        $('.undo').hide();
    });

    $(document).on("click", '#last-used .comment .edit-comment', function (event) {
        /*
         var comment = $(this).parent().children('textarea');
         var newcomment = prompt('Tweak this comment permanently', comment.html());
         if(newcomment){
         comment.html(newcomment);
         editComment($(this).parent().data('id'), newcomment);
         }
         */
        //
        var thisComment = $(this).parent('.comment');
        var thisTextarea = thisComment.children('textarea');
        selectIndexById(thisComment.data('id'));
        thisComment.addClass('being-edited');
        thisTextarea.removeAttr('readonly');
        thisTextarea.focus();
        thisComment.children('.edit-comment,.add-to-builder.constructive').hide();
        thisComment.children('.add-to-builder.positive').css('visibility', 'hidden');
        thisComment.children('.delete-comment, .save-changes').show();
    });

    $(document).on("click", '#last-used .comment .delete-comment', function (event) {
        var bool = confirm("Are you sure you want to delete this comment?");
        if (bool) {
            $(this).tooltip('hide');
            var id = $(this).parent().data('id');
            deleteComment(id);
            $('#builder .comment[data-id="' + id + '"]').remove();
            $(this).parent().remove();
            //removeFromSearch();
        }

    });

    $(document).on("click", '#last-used .comment .save-changes', function (event) {
        var id = $(this).parent().data('id');
        saveChanges(id);

    });

    $(document).on("click", '#builder .comment .remove-from-builder', function (event) {
        $(this).tooltip('hide');
        var id = $(this).parent().data('id');
        $(this).parent().remove();
        removeFromSearch();
        fixTextareas();
    });

    $(document).on("click", '#builder .comment .edit-comment', function (event) {
        /*
         var comment = $(this).parent().children('textarea');
         var newcomment = prompt('Tweak this comment as a one off', comment.html());
         if(newcomment){
         comment.html(newcomment);     
         }
         */
        var thisComment = $(this).parent('.comment');
        var thisTextarea = thisComment.children('textarea');
        thisComment.addClass('being-edited');
        thisTextarea.removeAttr('readonly');
        thisTextarea.focus();
        thisComment.children('.edit-comment').hide();
        //thisComment.children('.add-to-builder.constructive').css('visibility','hidden');
        thisComment.children('.save-changes').show();
    });

    $(document).on("click", '#builder .comment .save-changes', function (event) {
        var thisComment = $(this).parent('.comment');
        var thisTextarea = thisComment.children('textarea');
        thisComment.removeClass('being-edited');
        thisTextarea.attr('readonly', true);
        thisTextarea.focus();
        thisComment.children('.edit-comment').show();
        //thisComment.children('.add-to-builder.constructive').css('visibility','hidden');
        thisComment.children('.save-changes').hide();
        selectIndex(0);
    });

    $('.open-popup-link').magnificPopup({
        type: 'inline',
        midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
    });
    $('.reset').click(function () {
        archiveBuilder();
        $('#builder .comment, #last-used .comment').remove();
        $('#builder h4').hide();
        emptyInput();
    });
    $('.undo').click(function () {
        $('#builder .feedback').html(archive);
        $(this).tooltip('hide').hide();
        applyAjaxTooltips();
    });
    $('.pastalink').click(function () {
        $('#pasta').val(generatePasta());
    });
});

function submitFocusedBuilder() {
    var submitted = false;

    $('#builder .comment.being-edited textarea').each(function () {
        if ($(this).is(':focus')) {
            $(this).parent('.comment').children('.btn.save-changes').click();
            submitted = true;
        }
    });

    return submitted;
}

function selectIndex(index) {
    $("#last-used .comment.highlighted").removeClass("highlighted positive constructive");
    $("#last-used .comment textarea:focus").blur();
    var beingEdited = $("#last-used .comment.being-edited");
    if (beingEdited.length) {
        var beingEditedID = beingEdited.data('id');
        saveChanges(beingEditedID);
    }

    if (index < 0) {
        index = 0;
    }

    var numActiveComments = $("#last-used .comment:visible").length;

    if (index > numActiveComments) {
        index = numActiveComments;
    }

    selectedIndex = index;
    if (selectedIndex === 0) {
        $('input[name="input"]').focus();
    } else {
        $("#last-used .comment:visible").each(function (i) {
            if (i + 1 === index) {
                $(this).addClass("highlighted");
                $(this).children("textarea").focus();
            }
        });
        $('input[name="input"]').blur();
    }
}
function selectIndexById(id) {
    var i = 1;
    $("#last-used .comment:visible").each(function () {
        if ($(this).data('id') === id) {
            selectIndex(i);
        } else {
            i = i + 1;
        }
    });
}
function selectType(type) {
    if ($("#last-used .comment.highlighted")[0]) {
        $("#last-used .comment.highlighted").removeClass("positive constructive");

        $("#last-used .comment.highlighted").addClass(type).data('type', type);

        applyAjaxTooltips();
        $("#last-used .comment.highlighted .add-to-builder." + type).tooltip('show');
    }
}
function addHighlighted() {
    var highlighted = $("#last-used .comment.highlighted");
    if (highlighted[0]) {
        updateLastUsed(highlighted.data('id'));
        addToBuilder(highlighted.data('id'), highlighted.children('textarea').text(), highlighted.data('type'));
        emptyInput();
    }
}

function updateLastUsed(id) {
    $.post('/aj_update_last_used', {id: id});
}
function deleteComment(id) {
    $.post('/aj_delete', {id: id});
}
function saveChanges(id) {
    var thisComment = $("#last-used .comment[data-id='" + id + "']");
    var thisTextarea = thisComment.children('textarea');
    editComment(id, thisTextarea.val());
    thisComment.removeClass('being-edited');
    thisTextarea.attr('readonly', 'readonly');
    thisTextarea.focus();
    thisComment.children('.delete-comment, .save-changes').hide();
    thisComment.children('.edit-comment,.add-to-builder.constructive').show();
    thisComment.children('.add-to-builder.positive').css('visibility', 'visible');
}
function editComment(id, comment) {
    $.post('/aj_edit', {id: id, comment: comment});
}

function addToBuilder(id, comment, type) {
    var del_button = '<button class="remove-from-builder btn btn-secondary"><i class="fa fa-times" aria-hidden="true"></i></button>';
    var edit_button = '<button class="edit-comment btn btn-secondary"><i class="fa fa-pencil" aria-hidden="true"></i></button>';

    var save_button = '<button class="save-changes btn btn-secondary"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>';


    var html = "<aside data-id='" + id + "' data-type='" + type + "' class='comment " + type + "'><textarea readonly>" + comment + "</textarea>" + del_button + edit_button + save_button + "</aside>";
    $('#builder #' + type).append(html);
    $('#builder #' + type + ' h4').show();
    fixTextareas();
    applyAjaxTooltips();
}

function removeFromSearch() {
    var ids = addedIds();

    $('#last-used .comment').each(function () {
        if (ids.indexOf($(this).data('id')) != -1) {
            $(this).hide();
        } else {
            $(this).show();
        }
    });
}

function addedIds() {
    var ids = [];
    $('#builder .comment').each(function () {
        ids.push($(this).data('id'));
    });
    return ids;
}

function retrieveComments(search, selectfirst = false, tooltip = '') {
    if (search !== undefined && search !== "") {
        $.post("/aj_get", {input: search})
                .done(function (data) {
                    $('#last-used .comment').remove();

                    var obj = data;
                    $.each(obj, function (key) {
                        var thiscomment = obj[key];
                        if (thiscomment.comment_type == 1) {
                            var type = 'positive';
                        } else {
                            var type = 'constructive';
                        }
                        /*
                         if(type==="positive"){
                         var inverse = "constructive";
                         }else{
                         var inverse = "positive";
                         }
                         */
                        var pos_button = '<button class="add-to-builder positive btn btn-secondary" data-type="positive">&#10004;</button>';
                        var con_button = '<button class="add-to-builder constructive btn btn-secondary" data-type="constructive">&#10008;</button>';

                        var del_button = '<button class="delete-comment btn btn-secondary"><i class="fa fa-times" aria-hidden="true"></i></button>';
                        var edit_button = '<button class="edit-comment btn btn-secondary"><i class="fa fa-pencil" aria-hidden="true"></i></button>';

                        var save_button = '<button class="save-changes btn btn-secondary"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>';

                        var html = "<aside data-id='" + thiscomment.id + "' data-type='" + type + "' class='comment'>" + pos_button + "<textarea readonly>" + thiscomment.comment + "</textarea>" + con_button + edit_button + del_button + save_button + "</aside>"

                        $('#last-used').append(html);
                    });
                    applyAjaxTooltips();
                    removeFromSearch();
                    fixTextareas();
                    $(".loader").hide();
                    if (selectfirst) {
                        selectIndex(1);
                        if (tooltip) {
                            var options = {
                                title: tooltip,
                                placement: 'bottom',
                                trigger: 'manual',
                                html: true,
                                template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner" style="max-width:350px"></div></div>'
                            }
                            $('.comment.highlighted textarea').tooltip(options).tooltip('show');
                        }
                    }
                });
    } else {
        $(".loader").hide();
}
}
function archiveBuilder() {
    archive = $('#builder .feedback').html();
    $('.undo').show();
}
function applyAjaxTooltips() {
    $('.btn:not(.submit,.reset,.copylink,.undo)').each(function () {
        $(this).tooltip('dispose');
    });
    var editoptions = {
        title: 'Edit',
        placement: 'bottom',
        template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner" style="max-width:350px"></div></div>'
    };
    $('#last-used .edit-comment').each(function () {
        $(this).tooltip(editoptions);
    });
    var editoneoffoptions = {
        title: 'Tweak this comment as a one-off',
        placement: 'bottom',
        template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner" style="max-width:350px"></div></div>'
    };
    $('#builder .edit-comment').each(function () {
        $(this).tooltip(editoneoffoptions);
    });
    var positiveoptions = {
        title: 'Add as achievement',
        placement: 'bottom',
        template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner" style="max-width:350px"></div></div>'
    };
    $('.add-to-builder.positive').each(function () {
        $(this).tooltip(positiveoptions);
    });
    var constructiveoptions = {
        title: 'Add as target',
        placement: 'bottom',
        template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner" style="max-width:350px"></div></div>'
    };
    $('.add-to-builder.constructive').each(function () {
        $(this).tooltip(constructiveoptions);
    });
    var saveoptions = {
        title: 'Save changes to your Cloud',
        placement: 'bottom',
        template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner" style="max-width:350px"></div></div>'
    };
    $('#last-used .save-changes').each(function () {
        $(this).tooltip(saveoptions);
    });
    var saveoneoffoptions = {
        title: 'Confirm your changes',
        placement: 'bottom',
        template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner" style="max-width:350px"></div></div>'
    };
    $('#builder .save-changes').each(function () {
        $(this).tooltip(saveoneoffoptions);
    });
    var deloptions = {
        title: 'Permanently delete Comment',
        placement: 'bottom',
        template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner" style="max-width:350px"></div></div>'
    };
    $('.delete-comment').each(function () {
        $(this).tooltip(deloptions);
    });
    var remoptions = {
        title: 'Remove Comment from feedback',
        placement: 'bottom',
        template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner" style="max-width:350px"></div></div>'
    };
    $('.remove-from-builder').each(function () {
        $(this).tooltip(remoptions);
    });
}
function clearTooltips() {
    $('.comment textarea,.btn').each(function () {
        $(this).tooltip('hide');
    });
}
function fixTextareas() {
    $("textarea").each(function () {
        fixTextareaHeight(this);
    });
}

function fixTextareaHeight(textarea) {
    $(textarea).autogrow({vertical: true, horizontal: false});
    //$(textarea).height( $(textarea)[0].scrollHeight );
}

function emptyInput() {
    selectedIndex = 0;
    $('input[name="input"]').val('');//empty out input after successful add
    $('input[name="input"]').focus();
    retrieveComments(undefined);
}

function generatePasta() {
    var string = "You have shown that you can:";
    string = string + pasta('positive');

    string = string + "\n\nTo improve, you should:";
    string = string + pasta('constructive');

    return string;
}

function pasta(type) {
    var counter = 0;
    var string = '';
    var total = $('#builder #' + type + ' .comment').length;
    $('#builder #' + type + ' .comment').each(function () {
        //if ($(this).data('type') === "positive") {
        counter++;
        if (counter > 1) {
            string = string + ";";
        }
        if (counter > 1 && counter === total) {
            string = string + " and";
        }
        string = string + "\n• " + $(this).children('textarea').val();
        //}
    });

    if (counter > 0) {
        string = string + ".";
    }
    return string;
}


function copyToClipboardFF(text) {
    window.prompt("Copy to clipboard: Ctrl C, Enter", text);
}

function copyToClipboard() {
    var success = true,
            range = document.createRange(),
            selection;
    var copyBtn = $(".copylink"),
            input = $("#pasta");
    // For IE.
    if (window.clipboardData) {
        window.clipboardData.setData("Text", input.val());
    } else {
        // Create a temporary element off screen.
        var tmpElem = $('<div>');
        tmpElem.css({
            position: "absolute",
            left: "-1000px",
            top: "-1000px",
        });
        // Add the input value to the temp element.
        tmpElem.html(input.val().replace(/\n/g, "<br />"));
        $("body").append(tmpElem);
        // Select temp element.
        range.selectNodeContents(tmpElem.get(0));
        selection = window.getSelection();
        selection.removeAllRanges();
        selection.addRange(range);
        // Lets copy.
        try {
            success = document.execCommand("copy", false, null);
        } catch (e) {
            copyToClipboardFF(input.val());
        }
        if (success) {
            //alert ("The text is on the clipboard, try to paste it!");
            // remove temp element.
            tmpElem.remove();
            var copylink = $('.copylink');
            //copylink.css('background-color','lightgreen');
            copylink.children('i').removeClass('fa-files-o').addClass('fa-clipboard');
            var originaltitle = copylink.attr('data-original-title');
            copylink.attr('data-original-title', 'Copied! You can now paste your comments.');
            copylink.tooltip('dispose');
            copylink.tooltip('show');
            setTimeout(function () {
                //$('.copylink').text('Copy to clipboard').removeAttr('style');
                copylink.removeAttr('style');
                copylink.children('i').removeClass('fa-clipboard').addClass('fa-files-o');
                //copylink.tooltip('hide');
                copylink.attr('data-original-title', originaltitle);
                copylink.tooltip('dispose').tooltip();
            }, 2000);
        }
    }
}

(function ($, undefined) {
    $.fn.getCursorPosition = function () {
        var el = $(this).get(0);
        var pos = 0;
        if ('selectionStart' in el) {
            pos = el.selectionStart;
        } else if ('selection' in document) {
            el.focus();
            var Sel = document.selection.createRange();
            var SelLength = document.selection.createRange().text.length;
            Sel.moveStart('character', -el.value.length);
            pos = Sel.text.length - SelLength;
        }
        return pos;
    }
})(jQuery);  