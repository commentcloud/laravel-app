<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public static function get_last($limit,$user_id,$search = null){
        if($search){
            $comments = Comment::where('user_id',$user_id)
                ->where('comment','like','%'.$search.'%')
                ->orderBy('updated_at','desc')
                ->take($limit)
                ->get();
            
        }else{
            $comments = Comment::where('user_id',$user_id)
                ->orderBy('updated_at','desc')
                ->take($limit)
                ->get();
            
        }
        
        return $comments;
                 
    }
}
