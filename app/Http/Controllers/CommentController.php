<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Auth;

class CommentController extends Controller {
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        $comments = Comment::all();

        return view('comments.index', compact('comments'));
    }

    public function show(Comment $comment) {

        return view('comments.show', compact('comment'));
    }

    public function aj_get(Request $request) {

        if (Auth::check()) {
            $user_id = Auth::id();

            //@TODO - verify user, input, etc.
            $comment = $request->input('input');

            $comments = Comment::get_last('20', $user_id, $comment);

            return $comments;
        }
    }

    public function aj_add(Request $request) {
        if (Auth::check()) {
            $user_id = Auth::id();

            $input = $request->input('input');
            if (empty($input)) {
                return 'no input';
            } else {
                $comment = new Comment;
                $comment->comment = $input;
                $comment->user_id = $user_id;
                $comment->save();

                return $comment->id;
            }
        }
    }

    public function aj_update_last_used(Request $request) {
        $id = $request->input('id');
        
        if ($this->check_ownership($id)) {

            $comment = Comment::find($id);
            $comment->touch();
        }
    }

    public function aj_delete(Request $request) {
        $id = $request->input('id');
        
        if ($this->check_ownership($id)) {

            $comment = Comment::find($id);
            $comment->delete();
        }
    }

    public function aj_edit(Request $request) {
        $id = $request->input('id');
        
        if ($this->check_ownership($id)) {
            $input = $request->input('comment');
            if (!empty($input)) {
                $comment = Comment::find($id);
                $comment->comment = $input;
                $comment->save();
            }
        }
    }
    
    function check_ownership($id){
        if (Auth::check()) {
            $user_id = Auth::id();
            $comment = Comment::where('id', $id)->first();
            
            if($user_id==$comment->user_id){
                return TRUE;
            }
        }
        
        return FALSE;
    }

}
